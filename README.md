# Kukano Android App

If you already discovered the kukano [rpi-client](https://gitlab.com/kukano/rpi) and the [backend](https://gitlab.com/kukano/backend)
you can also install and use this app for your phone. It's nothing fancy, it's just a thing I made on a lazy sunday afternoon.

## Handling the application

![printscreen](https://gitlab.com/kukano/android-app/-/raw/master/doc/printscreen.jpg)

With any new incident, you will get a new entry in the main screen. Each entry could be open as this:

![printscreen](https://gitlab.com/kukano/android-app/-/raw/master/doc/detail.jpg)

There you can download full resolution picture and also a picture from the thumbnail. The reason is that the thumbnail
picture also contains explanation of the detection. In this case, where you see red cross, it means the picture was not
uploaded properly.

## Setting up Firebase notifications

Any howto on Firebase notifications will work just fine, do not forget to add `android/app/google-services.json` file.
Once it's done, I just used plain old curl command to send the notification, and it could be setup at
[run_after_notification_script.sh](https://gitlab.com/kukano/rpi/-/blob/master/main.py#L312) and it could look somewhat
like this:

```bash
curl -H 'Content-Type: application/json' -H 'Authorization: key=KEY FROM FIREBASE' -X POST  https://fcm.googleapis.com/fcm/send --data '
{
  "to" : "THE FCM TOKEN GIVEN BY THE PHONE",
  "collapse_key" : "New Message",
  "priority": "high",
  "notification" : {
    "title": "Kukano alert!",
    "body" : "Somebody in da house!"
  },
  "data": {
    "title": "Kukano alert!",
    "body": "Somebody in da house"
  }
}
'
```
