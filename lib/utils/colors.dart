import 'dart:ui';

import 'package:flutter/material.dart';

class CustomColors {
  static const dividerColor = Color(0xFFF2F2F2);
  static const blueColor = Color(0xFF1600FF);
}
