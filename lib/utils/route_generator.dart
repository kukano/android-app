import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kukano/models/incident_model.dart';
import 'package:kukano/widgets/incident_detail_screen.dart';
import 'package:kukano/widgets/incident_list_screen.dart';
import 'package:kukano/widgets/not_found_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings){
    final args = settings.arguments;

    switch (settings.name) {
      case IncidentListScreen.routeName:
        return MaterialPageRoute(builder: (_) => IncidentListScreen());
      case IncidentDetailScreen.routeName:
        if (args is Incident)
          return MaterialPageRoute(
              settings: RouteSettings(name: IncidentDetailScreen.routeName),
              builder: (_) => IncidentDetailScreen(incident: args));
        return MaterialPageRoute(builder: (_) => NotFoundScreen());
      default:
        return MaterialPageRoute(builder: (_) => NotFoundScreen());
    }
  }
}
