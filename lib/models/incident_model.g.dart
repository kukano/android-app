// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incident_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Incident _$IncidentFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['id']);
  return Incident(
    timestamp: json['timestamp'] == null
        ? null
        : DateTime.parse(json['timestamp'] as String),
    picture: json['picture'] as String,
    fullPicture: json['full_picture'] as String,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$IncidentToJson(Incident instance) => <String, dynamic>{
      'timestamp': instance.timestamp?.toIso8601String(),
      'picture': instance.picture,
      'full_picture': instance.fullPicture,
      'id': instance.id,
    };
