import 'package:json_annotation/json_annotation.dart';

part 'incident_model.g.dart';

@JsonSerializable()
class Incident {
  final DateTime timestamp;
  final String picture;
  @JsonKey(name: "full_picture")
  final String fullPicture;
  @JsonKey(required: true)
  final int id;

  Incident({this.timestamp, this.picture, this.fullPicture, this.id});
  factory Incident.fromJson(Map<String, dynamic> json) => _$IncidentFromJson(json);
  Map<String, dynamic> toJson() => _$IncidentToJson(this);
}
