import 'dart:convert';

import 'package:kukano/models/incident_model.dart';
import 'package:http/http.dart' as http;

import 'api_constants.dart';


List<Incident> parseIncidents(String responsesBody){
  final parsed = jsonDecode(responsesBody).cast<Map<String, dynamic>>();
  return parsed.map<Incident>((json) => Incident.fromJson(json)).toList();
}


Future<List<Incident>> fetchIncidents() async {
  final response = await http.get(
      api_endpoint + '/incident/',
      headers: {
        "x-api-Key": api_key
      },
  );

  if (response.statusCode == 200) {
    var responseRecipes = parseIncidents(response.body);
    responseRecipes.sort((a, b) => b.timestamp.compareTo(a.timestamp));
    return responseRecipes;
  }else {
    throw Exception('Failed to load incidents');
  }
}

void deleteIncident(int id) async {
  final response = await http.delete(
    api_endpoint + '/incident/' + id.toString(),
    headers: {
      "x-api-Key": api_key
    },
  );

  if (response.statusCode != 200) {
    throw Exception('Failed to load incidents');
  }
}


