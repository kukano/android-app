import 'dart:io';
import 'package:http/http.dart' show get;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

Future<String> getStorageDirectory() async {
  if (Platform.isAndroid) {
    return (await getExternalStorageDirectory()).path;  // OR return "/storage/emulated/0/Download";
  } else {
    return (await getApplicationDocumentsDirectory()).path;
  }
}

void fetchPicture (String imgUrl, String localName) async{
  var response = await get(imgUrl);
  File file = new File(
      join((await getStorageDirectory()), localName)
  );
  file.writeAsBytesSync(response.bodyBytes);
}