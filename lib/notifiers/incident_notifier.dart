import 'package:flutter/material.dart';
import 'package:kukano/backend/crud_incident.dart';
import 'package:kukano/models/incident_model.dart';

class IncidentNotifier extends ChangeNotifier {
  List<Incident> _incidents = [];

  void _fetchIncidents() async {
    _incidents = await fetchIncidents();
  }

  List<Incident> getIncidents(){
    _fetchIncidents();
    return _incidents;
  }

  void removeIncident(Incident incident) async {
    deleteIncident(incident.id);
    _fetchIncidents();
    notifyListeners();
  }
}
