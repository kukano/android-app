import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kukano/backend/crud_incident.dart';
import 'package:kukano/models/incident_model.dart';
import 'package:kukano/utils/colors.dart';

import 'incident_widget.dart';

Timer timer;

List<Incident> incidents = [];

class IncidentListScreen extends StatelessWidget {
  static const routeName = "/";

  IncidentListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _IncidentListBody()
    );
  }
}

class _IncidentListBody extends StatefulWidget {
  _IncidentListBody({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _IncidentListBodyState();
}

class _IncidentListBodyState extends State<_IncidentListBody> {

  @override
  void initState() {
    super.initState();
    _getIncidents();
    incidentsFuture = fetchIncidents();
    incidentsState = incidents;
    timer = Timer.periodic(Duration(seconds: 5), (Timer t) => rebuild());
  }

  Future <List<Incident>> incidentsFuture;
  List<Incident> incidentsState;

  _getIncidents() async {
    incidents = await fetchIncidents();
  }

  rebuild() async {
    await refresh();
  }

  Future refresh() async {
    print("called");
    _getIncidents();
    setState(() {
      incidentsFuture = fetchIncidents();
      incidentsState = incidents;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Incident>>(
      future: fetchIncidents(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return NotificationListener<ScrollNotification>(
            child: RefreshIndicator(
              child: ListView.separated(
                separatorBuilder: (context, position) {
                  return Divider(
                    color: CustomColors.dividerColor,
                    height: 3,
                    thickness: 3,
                    indent: 16
                  );
                },
                itemCount: incidentsState.length,
                itemBuilder: (context, index) {
                  return IncidentWidget(incident: incidentsState[index]);
                },
              ),
              onRefresh: refresh
            )
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        // By default, show a loading spinner.
        return Center(child: CircularProgressIndicator());
      }
    );
  }
}
