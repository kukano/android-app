import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kukano/models/incident_model.dart';
import 'package:kukano/utils/colors.dart';
import 'package:kukano/widgets/incident_detail_screen.dart';

class IncidentWidget extends StatelessWidget {
  final Incident incident;

  const IncidentWidget({Key key, this.incident}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String title = DateFormat('yyyy-MM-dd – kk:mm').format(incident.timestamp);
    return InkWell(
        onTap: () {
          Navigator.pushNamed(context, IncidentDetailScreen.routeName, arguments: incident);
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 32, vertical: 24),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(16),
                    child: Image.network(
                      incident.picture,
                      width: 100,
                      errorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                        return Image.asset('assets/not-available.jpeg', width: 100);
                      },
                    )
                ),
                SizedBox(width: 24,),
                Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 4),
                          Text(
                            title,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                                color: CustomColors.blueColor,
                                fontSize: 20,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                          SizedBox(height: 12)
                        ]
                    )
                )
              ],
            )
        )
    );
  }
}