import 'package:flutter/material.dart';

class NotFoundScreen extends StatelessWidget {
  static const routeName = "/not_found_screen";

  NotFoundScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Not found")), body: _NotFoundScreen());
  }
}

class _NotFoundScreen extends StatelessWidget {
  _NotFoundScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(child: Text("SCREEN NOT FOUND"));
  }
}
