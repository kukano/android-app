import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kukano/backend/crud_incident.dart';
import 'package:kukano/backend/fetch_picture.dart';
import 'package:kukano/models/incident_model.dart';
import 'package:kukano/utils/colors.dart';
import 'package:kukano/widgets/confirm_notice.dart';
import 'package:path/path.dart';


class IncidentDetailScreen extends StatelessWidget {
  final Incident incident;

  static const routeName = "/incident_detail_screen";

  const IncidentDetailScreen({Key key, this.incident}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(DateFormat('yyyy-MM-dd – kk:mm').format(incident.timestamp))),
        body: _IncidentDetailBody(incident: incident));
  }
}

class _IncidentDetailBody extends StatelessWidget {
  final Incident incident;

  _IncidentDetailBody({Key key, this.incident}) : super(key: key);

  void _askForDeleteAndRemove(BuildContext context) async {
    final ConfirmAction action = await asyncConfirmDeleteDialog(context);
    if (action == ConfirmAction.Accept){
      deleteIncident(incident.id);
      Navigator.pop(context);
    }
  }

  void _askForSaveInCasePathPresentAndFetch(BuildContext context, String pictureName, String pictureKey) async {
    if (File(join((await getStorageDirectory()), pictureName)).existsSync()){
      final ConfirmAction action = await asyncConfirmDownloadAgainDialog(context);
      if (action == ConfirmAction.Accept){
        fetchPicture(pictureKey, pictureName);
      }
    } else {
      fetchPicture(pictureKey, pictureName);
      asyncConfirmDialog(context);
    }
  }


  @override
  Widget build(BuildContext context) {
    String pictureName = DateFormat('yyyy-MM-dd_kk:mm.jpeg').format(incident.timestamp);
    String detailedPictureName = DateFormat('yyyy-MM-dd_kk:mm').format(incident.timestamp) + '_detailed.jpeg';

    return Center(
        child: Column(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child: Image.network(
                  incident.picture,
                  errorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                    return Image.asset('assets/not-available.jpeg');
                  },
                )
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue, // background
                    onPrimary: Colors.white, // foreground
                  ),
                  child: Text("Save image"),
                  onPressed: () {
                    _askForSaveInCasePathPresentAndFetch(context, pictureName, incident.picture);
                  }
              ),
              Divider(
                color: CustomColors.dividerColor,
                height: 3,
                thickness: 3,
                indent: 16
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue, // background
                    onPrimary: Colors.white, // foreground
                  ),
                  child: Text("Save detailed image"),
                  onPressed: () {
                    _askForSaveInCasePathPresentAndFetch(context, detailedPictureName, incident.fullPicture);
                  }
              ),
              Divider(
                color: CustomColors.dividerColor,
                height: 3,
                thickness: 3,
                indent: 16
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue, // background
                    onPrimary: Colors.white, // foreground
                  ),
                  child: Text("Delete"),
                  onPressed: () {
                    _askForDeleteAndRemove(context);
                  }
              )
            ],
          )
        ]
      )
    );
  }

}
